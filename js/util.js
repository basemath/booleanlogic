;(function(){
   var variables = [], operators = "()+*'"

   var isLetter = function(someString){
      return typeof someString == 'string' && someString.match(/[a-zA-Z]/)
   }

   var isOperator = function(someString){
      return operators.indexOf(someString) > -1
   }

   var reset = function(){
      variables = []
      $('#truthTable thead tr, #truthTable tbody').empty()
   }

   var calculateExpr = function(expression, map){
      var thisChar, var1, var2, operator,
      result = 0,
      operatorStack = [],
      operandStack = []

      var foobar = function(){
         operator = operatorStack.pop()
         var1 = operandStack.pop()
         var1 = isLetter(var1) ? map[var1] : var1
         if( operator == "'" ){
            result = 1 - var1
         }
         else{
            var2 = operandStack.pop()
            var2 = isLetter(var2) ? map[var2] : var2
            result = operator == '*' ? var1 * var2 : var1 + var2
         }
         operandStack.push(result)
      }

      for( var i=0; i<expression.length; i++ ){
         thisChar = expression.charAt(i)
         if( thisChar == '(' ){
            operatorStack.push(thisChar)
         }
         else if( isLetter(thisChar) ){
            operandStack.push(thisChar.toLowerCase())
            console.log('pushed '+thisChar+' to the operand stack')
         }
         else if( isOperator(thisChar) && thisChar != ')' ){
            if( operatorStack.length && operators.indexOf(operatorStack[operatorStack.length-1]) >= operators.indexOf(thisChar) ){
               foobar()
            }
            operatorStack.push(thisChar)
            console.log('pushed '+thisChar+' to the operator stack')
         }
         else if( thisChar == ')' ){
            while( operatorStack[operatorStack.length-1] != '(' ){
               foobar()
            }
            //now we have some empty parenthesis with nothing between them,
            //so we pop that last one off
            operatorStack.pop()
         }
      }

      //the end of the expression has been reached.
      //now we do blah blah blah
      while( operatorStack.length ){
         foobar()
      }

      result = operandStack.length ? operandStack.pop() : result
      return result > 0 ? 1 : 0
   }

   var build = function(){
      reset()
      var e1 = $('#expression1').val().trim()
      //if we have input...
      if( e1 ){
         var c, index
         //get all the variables
         for( var i in e1 ){
            c = e1[i]
            if( isLetter(c) ){
               c = c.toLowerCase()
               if( variables.indexOf(c) < 0 ){
                  variables.push(c)
               }
            }
         }

         if( variables.length > 6 ){
            alert('No more than 6 variables, please')
         }
         else{
            //make the header for the table
            variables.sort()
            for( var i=0; i<variables.length; i++ ){
               $('#truthTable thead tr').append('<th>'+variables[i]+'</th>')
            }
            $('#truthTable thead tr').append('<th>Output</th>')
            //build the truth table
            var val, n, row, map
            for( var i=0; i<Math.pow(2, variables.length); i++){
               map = {}
               n = 0
               row = "<tr>"
               for( var j=0; j<variables.length; j++ ){
                  val = (Math.floor(i / Math.pow(2, variables.length - n - 1))) % 2
                  map[variables[j]] = val
                  row += "<td>"+val+"</td>"
                  n++
               }
               val = calculateExpr(e1, map)
               row += "<td>"+val+"</td></tr>"
               $('#truthTable tbody').append(row)
            }
         }
      }
      return false
   }

   $('document').ready(function(){
      $('#go').click(build)
   })
})()
